/**
 * Copyright 2019-present Kiapan Project developers. Do not redistribute without
 * written permission from authors.
**/

'use strict';

class DateTime extends Date {
  constructor() {
    super();
  }

  static time() {
    let d = new Date();

    return d.getHours() + ":"  
          +d.getMinutes() + ":" 
          +d.getSeconds();
  }
}


interface Msg {
  msg: string
}

class AbstractNode {
  node: HTMLElement;
  template: string;
  constructor(elem: HTMLElement) {
    this.template = elem.innerHTML;
    this.node = elem;
  }

  static escape_msg(raw: Msg) {
    raw.msg = AbstractNode.escape(raw.msg)

    return raw;
  }

  static escape(raw: string) {
    return raw
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/&/g, "&amp;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
  }

  static render_string(template: string, obj: Object) {
    // woah this is unreadable
    // what was I thinking when i wrote this
    // it works tho so im not gonna mess with it.
    return template.replace(/\{\{\s*(\w+)\s*\}\}/g, (_: any, v: any) => 
      v.split('.').reduce((p: any, c: any) => p[c], obj) || '')
      .replace(/\{%\s*if\s+(\w+)\s+([^%]+)\s*%\}/g, (_: any, bool: any, rest: any): string =>
        bool.split('.').reduce((p:any,c:any) => p[c], obj) ? AbstractNode.render_string(rest, obj) : '')
  }

  render(obj: Object | Array<any>): boolean {
    if (obj instanceof Array) {
      this.node.innerHTML = obj.reduce((p: string, c: string) => p + AbstractNode.render_string(this.template, c), '');
      return true;
    } else if (obj instanceof Object) {
      this.node.innerHTML = AbstractNode.render_string(this.template, obj);
      return true;
    }
    return false;
  }
}

