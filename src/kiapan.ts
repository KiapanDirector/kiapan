/*
 * Copyright 2019-present Kiapan Project developers. Do not redistribute without
 * written permission from authors.
 */

'use strict';

import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import * as http from 'http';
import * as socket from 'socket.io'
import * as jwt from 'jsonwebtoken';
import {v4} from 'uuid';

let config = {
  port: Number(process.env.PORT) || 8080,
  secret: process.env.GFC_SECRET || "GFC_SECRET"
};

const app = express()
const www = new http.Server(app)
const io = socket(www, {
  // limit max size to 128kb to avoid DoS attacks
  maxHttpBufferSize: 128 * 1024,
  pingTimeout: 1000 * 2
})

app.use(cookieParser());

const static_routes = {
  "/auth": "/dist/auth.js",
  "/login": "/html/login.html",
  "/": "/html/index.html",
  "/css": "/static/main.css",
  "/js": "/dist/client.js",
  "/frontend": "/dist/frontend.js",
  "/socket": "/node_modules/socket.io-client/dist/socket.io.slim.js"
}

class DateTime extends Date {
  static time() {
    let d = new Date();

    return d.getHours() + ":"  
          +d.getMinutes() + ":" 
          +d.getSeconds();
  }
}

for (let [k, v] of Object.entries(static_routes)) {
  app.get(k, (_, res) => {
    res.sendFile(process.env.PWD + v);
  })
}

app.get('/gfc/:secret', (req, res) => {
  let s = req.params['secret'];
  let t = jwt.sign({
    uuid: v4(),
    iat: Math.floor(Date.now() / 1000)
  }, s);
  res.send(s == config.secret ? t : "nice try pal");
})

interface Message {
  sender: string,
  msg: string,
  gfc: string,
  timestamp: string
}

const validate_msg = (gfc: string) => {
  try {
    return jwt.verify(gfc, config.secret)
  } catch {
    return undefined
  }
}

app.get('/chat', (req, res) => {
  let gfc = req.cookies.gfc;
  let valid = validate_msg(gfc);
  if (valid) {
    res.sendFile(process.env.PWD + '/html/chat.html');
  } else {
    res.redirect('/login');
  }
})

const desc = (i: string) => i.split('-')[0];

io.on('connection', socket => {
  console.log('connected!')
  socket.on('msg', (msg: Message) => {
    console.log("Received msg:", msg);
    let decoded: any = validate_msg(msg.gfc);
    if (decoded && msg.msg.trim() != '') {
      let de = desc(decoded.uuid);
      console.log("decoded:", decoded);
      console.log(`${msg.sender}: ${msg.msg}`);
      socket.broadcast.emit('msg', {
        timestamp: DateTime.time(),
        msg: msg.msg,
        sender: msg.sender || "anon",
        descrim: de
      });
    } else {
      console.log('Failed auth attempt');
    }
  });

  socket.on('descrim', (msg: Message) => {
    let valid: any = validate_msg(msg.gfc);
    socket.emit('descrim', valid ? desc(valid.uuid) : "Invalid GFC");
  });
})

www.listen(config.port, () => {
  console.log("Starting on", config.port)
})
