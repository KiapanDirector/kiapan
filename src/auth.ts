let $auth = document.getElementById('acct');

console.log('uname', Cookies.get('uname'));

$auth.addEventListener('submit', e => {
  let $uname  = document.getElementById('uname');
  let $gfc    = document.getElementById('gfc');
  console.log("Uname", $uname);
  e.preventDefault()

  Cookies.set('uname', $uname.value || '');
  Cookies.set('gfc', $gfc.value);
  window.location.replace('/chat');
});
