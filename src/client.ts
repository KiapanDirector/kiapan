/*
 * Copyright 2019-present Kiapan Project developers. Do not redistribute without
 * written permission from authors.
 */

'use strict';

let socket    = io();
let $acct     = document.getElementById('acct-stat');
let $form     = document.getElementById('input');
let $input    = document.getElementById('msgbox');
let $messages = document.getElementById('messages');


var messages = [];
var msg_node = new AbstractNode($messages);
var auth_status = {
  uname: Cookies.get('uname'),
  logged_in: Cookies.get('uname') != undefined && Cookies.get('uname') != '',
  descrim: 'loading...'
};

var auth_node = new AbstractNode($acct);

if (auth_status.logged_in) {
  socket.emit('descrim', {
    gfc: Cookies.get('gfc')
  });
}



auth_node.render(auth_status);


$form.addEventListener('submit', e => {
  console.log("submitted", $input.value);
  e.preventDefault();
  if ($input.value.trim() != '') {
    messages.push({
      msg: $input.value,
      sender: "me",
      me: true,
      timestamp: DateTime.time(),
      descrim: undefined
    })
  }

  msg_node.render(messages);
  let payload = {
    msg: $input.value,
    sender: Cookies.get('uname'),
    gfc: Cookies.get('gfc') || 'NO GFC PROVIDED',
    timestamp: DateTime.time()
  };
  console.log("Payload:", payload);
  socket.emit('msg', payload);
  $input.value = "";
  return false;
}, true);

socket.on('msg', msg => {
  console.log("got", msg);
  messages.push({
    msg: msg.msg,
    sender: msg.sender,
    timestamp: msg.timestamp,
    descrim: msg.descrim,
    mentioned: auth_status.logged_in 
      && msg.msg.includes(`${auth_status.uname}@${auth_status.descrim}`),
    me: false
  });
  msg_node.render(messages);
})

socket.on('descrim', desc => {
  auth_status.descrim = desc;
  auth_node.render(auth_status);
})

msg_node.render(messages);
